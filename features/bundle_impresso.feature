#language: pt

Funcionalidade: Validar cenarios da sprint 52

    Contexto: Estar na página de login do usuário
        Dado que eu visito "grupo_gen"

    @carrinho_bundle
    Cenario: Carrinho de compra bundle
        E eu busco e seleciono um produto bundle
        Entao eu adiciono produto bundle ao carrinho 
    
    @frete
    Cenario: valida frete do produto bundle
        E eu busco e seleciono um produto bundle
        Entao eu digito um cep e faco um calculo de entrega

    @abas
    Cenario: Abas produto bundle
        E eu busco e seleciono um produto bundle
        Entao eu clico nas abas do produto

    @obra
    Cenario: Validar o Resumo da obra do bundle
        E eu busco e seleciono um produto bundle
        Entao eu verifico se tem o resumo da obra

    @botao_compre_agora
    Cenario: Validar botão Compre agora
        E eu busco e seleciono um produto bundle
        E eu clico em compre agora  
        Entao eu faco login e abre a tela de endereço

    @valores
    Cenario: Validar valores do bundle
        E eu busco e seleciono um produto bundle
        Entao eu verifico se esta certo o valor do bundle

    @fluxo_bundle
    Cenario: Fluxo de compra de um produto bundle
        E eu busco e seleciono um produto bundle
        Entao eu clico em compre agora, faço login e finalizo pedido
 
    