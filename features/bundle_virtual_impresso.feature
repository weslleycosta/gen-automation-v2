#language: pt

    @virtual_impresso
    Funcionalidade: Validar bundle virtual + impresso
        
        Contexto: Estar na página de login do usuário
        Dado que eu visito "grupo_gen"

        @carrinho_virtual_impresso
        Cenario: Carrinho de compra
            E eu busco e seleciono um produto bundle virtual + impresso
            Entao eu adiciono produto bundle ao carrinho 

        @frete_virtual_impresso
        Cenario: valida frete do produto bundle
            E eu busco e seleciono um produto bundle virtual + impresso
            Entao eu digito um cep e faco um calculo de entrega

        @abas_virtual_impresso
        Cenario: Abas produto bundle
            E eu busco e seleciono um produto bundle virtual + impresso
            Entao eu clico nas abas do produto virtual e impresso

        @fluxo_bundle_virtual_impresso
        Cenario: Fluxo de compra de um produto bundle
            E eu busco e seleciono um produto bundle virtual + impresso
            Entao eu clico em compre agora, faço login e finalizo pedido

        @resumo_virtual_impresso
         Cenario: Validar o Resumo da obra do bundle
            E eu busco e seleciono um produto bundle virtual + impresso
            Entao eu verifico se tem o resumo da obra virtual + impresso

        @botao_compre_agora_vi
        Cenario: Validar botão Compre agora
            E eu busco e seleciono um produto bundle virtual + impresso
            E eu clico em compre agora
            Entao eu faco login e abre a tela de endereço

        @entrega_vi
        Cenario: Validar valores do bundle
            E eu busco e seleciono um produto bundle virtual + impresso
            Entao eu clico em compre agora, faço login e finalizo pedido