#language: pt

    @cadastro
    Funcionalidade: Fluxo de cadastro de usuário

        Contexto: Estar na página do gen
            Dado que eu visito "grupo_gen"

    @cadastro_fisico
        Cenario: Cadastrando pessoa física
            Dado eu clico em entrar
            E eu clico em criar conta para criar conta fisica
            Entao preencho os dados cadastrais de um cliente fisico

    @cadastro_juridico
        Cenario: Cadastrando pessoa jurídica
            Dado eu clico em entrar 
            E eu clico em criar conta para criar conta juridica
            Entao preencho os dados cadastrais de um cliente juridico