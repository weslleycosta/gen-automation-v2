#language: pt
@fluxo
Funcionalidade: Validar fluxos de compra

    Contexto: Estar na página gen
        Dado que eu visito "grupo_gen"

    @cenario1
    Cenario: Novo cliente; navega pela área; compra ebook; adiciona carrinho; cartão
        E eu filtro livro e-book e adiciono ao carrinho
        E eu crio novo cliente
        E finalizo pedido com cartao e consulto meus pedidos
        Entao verifico entrega automática

    @cenario2
    Cenario: Novo cliente; busca isbn; compra impresso; adiciona carrinho; cartao
        E eu busco livro por ISBN, consulto cep e adiciono ao carrinho na PDP
        E fecho pedido, crio novo cliente e preencho endereço de entrega igual de cobrança
        Entao finalizo venda com cartao e consulto meus pedidos

    @cenario3
    Cenario: Novo cliente; busca por título; compra curso; compre agora; entrega por email; cartão 
        E eu busco livro por título, seleciono produto e seleciono entrega por e-mail
        E clico em compre agora, crio novo cliente 
        Entao finalizo venda com cartao e consulto meus pedidos com entrega por e-mail

    @cenario4
    Cenario: Novo cliente; carrinho misto; adicionar carrinho; compre agora; endereço diferente de cobrança; salvar cartão
        E crio novo cliente, seleciono livro pelo adicionar carrinho e insiro e-book por compre agora
        E preencho o endereco, seleciono endereco de entrega diferente do de cobrança e preencho dados
        Entao seleciono pra salvar cartao, finalizo pedido,  verifico endereços diferentes e consulto meus pedidos
    
    @cenario5
    Cenario: Cliente logado; adicionar carrinho; visualiza carrinho; quantidade 2; entrega por e-mail; finaliza cartão
        E faço login, adiciono item ao carrinho, visualizo carrinho e quantidade igual a 2
        Entao seleciono entrega por e-mail e finalizo pedido com cartao