  Dado("eu clico em entrar") do
    @cadastro.entrar
  end
  
  Dado("eu clico em criar conta para criar conta fisica") do
    @cadastro.criar_conta
  end
  
  Dado("preencho os dados cadastrais de um cliente fisico") do
    @cadastro.cadastro_fisico
  end

  Dado("eu clico em criar conta para criar conta juridica") do
    @cadastro.criar_conta
  end

  Dado("preencho os dados cadastrais de um cliente juridico") do
    @cadastro.cadastro_juridico
  end

  Dado("clico em entrar e crio um customer") do
    @cadastro.entrar
    @cadastro.criar_conta
    @cadastro.cadastro_fisico
  end

  Dado('faço o cadastro de um customer') do
    @cadastro.cadastre
    @cadastro.cadastro_fisico
  end