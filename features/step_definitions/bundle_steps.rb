Dado("eu busco e seleciono um produto bundle") do
    @busca.busca_bundle
    sleep 3
    @catalogo_busca.oferta_bundle
end

Dado("eu busco e seleciono um produto bundle virtual + virtual") do
    @busca.virtual_virtual
    sleep 3
    @catalogo_busca.bundle_virtual
end

Dado('eu busco e seleciono um produto bundle virtual + impresso') do
    @busca.virtual_impresso
    sleep 3
    @catalogo_busca.bundle_virtual_impresso
end

Dado ("eu adiciono produto bundle ao carrinho") do
    @carrinho.adicionar_bundle
end

Dado ("eu clico nas abas do produto") do
    @produtos.abas
end

Dado ("eu clico nas abas do produto virtual e impresso") do
    @produtos.abas_virtual_impresso
end

Entao ("eu verifico se tem o resumo da obra") do
    assert_text('vgfasdfdasfsdafasdfds', wait: 10) 
end

Entao ("eu verifico se tem o resumo da obra virtual") do
    assert_text('RESUMO DO RESUMO DO RESUMO DO RESUMO DO RESUMO DO RESUMO', wait: 10) 
end

Entao ("eu verifico se tem o resumo da obra virtual + impresso") do
    assert_text('RESUMO DOS RESUMOS RESUMINDO TUDO QUE OS RESUMOS PODE RESUMIR', wait: 10) 
end

Entao ("eu clico em compre agora") do
    @produtos.compre_agora
end

Entao ("eu faco login e abre a tela de endereço") do
    @login.login
    assert_text('Endereço de Entrega', wait: 10)
end

Entao ("eu faco login e abre a tela de pagamento") do
    @login.login
    assert_text('PAGAMENTO', wait: 10)
end

Entao ("eu verifico se esta certo o valor do bundle") do
    assert_text('R$ 600,00', wait: 10)
    assert_text('ou em até 10x de R$60,00', wait: 10)
end

Entao ("eu verifico se esta certo o valor do bundle virtual") do
    assert_text('R$ 378,00', wait: 10)
    assert_text('ou em até 08x de R$47,25', wait: 10)
end

Entao ("eu clico em compre agora, faço login e finalizo pedido") do
    @produtos.compre_agora
    @login.login
    sleep 10
    @entrega.frete_entrega
    @pagamento.boleto
end

Entao ("eu clico em compre agora, faço login e finalizo pedido com bundle virtual") do
    @produtos.compre_agora
    @login.login
    @pagamento.boleto
end

Entao ("Seleciono entrega por e-mail e finalizo pedido") do
    @carrinho.entrega_email
    @carrinho.adicionar_bundle
    assert_text('Entrega por e-mail', wait: 10)
    sleep 3
    @carrinho.botao_continuar
    @login.login
    @carrinho.botao_continuar
    @entrega.selecionar_endereco
    @pagamento.boleto
    @pagamento.finalizar_boleto
end

Entao("eu digito um cep e faco um calculo de entrega") do
    @produtos.frete
end

Entao('eu clico nas abas do produto virtual - virtual') do
    @produtos.abas_virtual_virtual
end
