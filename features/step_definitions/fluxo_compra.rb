Dado('que eu visito {string}') do |string|
  url = DATA[string]
  visit(url)
end

Entao('eu filtro livro e-book e adiciono ao carrinho')do
  @busca.busca_titulo
  @produtos.selecionando_oferta_filtrada
  @carrinho.clicando_carrinho
  @carrinho.fechar_pedido
end

Entao('eu crio novo cliente')do
  @cadastro.cadastre
  @cadastro.cadastro_fisico
end

Entao('finalizo pedido com cartao e consulto meus pedidos')do
  @entrega.cadastro_endereco_ebook
  @pagamento.inserir_cartao
  @pagamento.termo_fazer_pedido
  @success.meus_pedidos
end

Entao('eu busco livro por ISBN, consulto cep e adiciono ao carrinho na PDP')do
  @busca.isbn_impresso
  @catalogo_busca.produto_isbn
  @catalogo_busca.frete
  @catalogo_busca.calcular_frete
  @catalogo_busca.adicionar_produto
end

Entao('fecho pedido, crio novo cliente e preencho endereço de entrega igual de cobrança')do
  @carrinho.clicando_carrinho
  @carrinho.fechar_pedido
  @cadastro.cadastre
  @cadastro.cadastro_fisico
  @entrega.endereco_impresso
  @entrega.frete_entrega
end

Entao('finalizo venda com cartao e consulto meus pedidos')do
  @pagamento.seleciona_cartao
  @pagamento.inserir_cartao
  @pagamento.termo_fazer_pedido
  @success.meus_pedidos
end

Entao('finalizo venda com cartao e consulto meus pedidos com entrega por e-mail')do
  @pagamento.inserir_cartao
  @pagamento.termo_fazer_pedido
  @success.meus_pedidos
  @success.detalhes
  assert_text('Entrega por e-mail', wait: 10)
end

Entao('eu busco livro por título, seleciono produto e seleciono entrega por e-mail')do
  @busca.busca_titulo
  @catalogo_busca.produto_ebook
  @carrinho.entrega_email
end

Entao('clico em compre agora, crio novo cliente')do
  @produtos.compre_agora
  @cadastro.cadastre
  @cadastro.cadastro_fisico
  @entrega.cadastro_endereco_ebook
end

Entao('crio novo cliente, seleciono livro pelo adicionar carrinho e insiro e-book por compre agora')do
  @cadastro.entrar
  @cadastro.criar_conta
  @cadastro.cadastro_fisico
  @busca.isbn_impresso
  @catalogo_busca.produto_isbn
  @catalogo_busca.adicionar_produto
  @busca.busca_titulo
  @catalogo_busca.produto_ebook
  @produtos.compre_agora
end

Entao ('preencho o endereco, seleciono endereco de entrega diferente do de cobrança e preencho dados')do
  @entrega.endereco_impresso
  @entrega.frete_entrega
  @entrega.endereco_diferente_cobranca
  @entrega.cadastro_endereco_ebook
end

Entao('seleciono pra salvar cartao, finalizo pedido,  verifico endereços diferentes e consulto meus pedidos')do
  @pagamento.seleciona_cartao
  @pagamento.salvar_cartao
  @pagamento.inserir_cartao
  @pagamento.termo_fazer_pedido
  @success.minha_conta
end

Entao('verifico entrega automática')do
  @success.detalhes
  assert_text('Entrega automática')
end

Entao('faço login, adiciono item ao carrinho, visualizo carrinho e quantidade igual a 2')do
  @cadastro.entrar
  @login.login
  @busca.isbn_impresso
  @catalogo_busca.produto_isbn
  @catalogo_busca.adicionar_produto
  @home.clica_logo
  @busca.isbn_virtual
  @catalogo_busca.produto_ebook
  @catalogo_busca.adicionar_produto
  @carrinho.clicando_carrinho
  @carrinho.quantidade_itens
end

Entao('seleciono entrega por e-mail e finalizo pedido com cartao')do
  @carrinho.entrega_email
  @carrinho.botao_continuar
  @entrega.frete_entrega
  @pagamento.seleciona_cartao
  @pagamento.inserir_cartao
  @pagamento.termo_fazer_pedido
end