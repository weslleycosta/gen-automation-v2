#language: pt

    @bundle_virtual
    Funcionalidade: Validar bundle virtual + virtual
        
        Contexto: Estar na página de login do usuário
        Dado que eu visito "grupo_gen"

        @carrinho_virtual
        Cenario: Carrinho de compra
            E eu busco e seleciono um produto bundle virtual + virtual
            Entao eu adiciono produto bundle ao carrinho 

        @abas_virtual
        Cenario: Abas produto bundle
            E eu busco e seleciono um produto bundle virtual + virtual
            Entao eu clico nas abas do produto virtual - virtual

        @obra_virtual
        Cenario: Validar o Resumo da obra do bundle
            E eu busco e seleciono um produto bundle virtual + virtual
            Entao eu verifico se tem o resumo da obra virtual

        @compre_agora_virtual
        Cenario: Validar botão Compre agora
            E eu busco e seleciono um produto bundle virtual + virtual
            E eu clico em compre agora  
            Entao eu faco login e abre a tela de pagamento

        @entrega_email
        Cenario: Validar valores do bundle
            E eu busco e seleciono um produto bundle virtual + virtual
            Entao Seleciono entrega por e-mail e finalizo pedido

        @fluxo_bundle_virtual
        Cenario: Fluxo de compra de um produto bundle
            E eu busco e seleciono um produto bundle virtual + virtual
            Entao eu clico em compre agora, faço login e finalizo pedido com bundle virtual