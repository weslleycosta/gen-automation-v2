require 'selenium-webdriver'
require 'base64'

Before do |scn|
    @pagamento = Pagamento.new
    @login = Login.new
    @entrega = Entrega.new
    @carrinho = Carrinho.new
    @cadastro = Cadastro.new
    @produtos = Produtos.new
    @busca = Busca.new
    @catalogo_busca = CatalogoBusca.new
    @home = Home.new
    @success = Sucesso.new

Capybara.current_session.driver.browser.manage.delete_all_cookies
page.driver.browser.manage.window.resize_to(1250, 720)
end

After do |scn|
    
    binding.pry if ENV['debug']
    
        
    shot_file = page.save_screenshot("log/screenshot.png")
    shot_b64 = Base64.encode64(File.open(shot_file, "rb").read)
    embed(shot_b64, "image/png","Screenshot") 
end

# After do
#     shot_file = page.save_screenshot("log/screenshot.png")
#     shot_b64 = Base64.encode64(File.open(shot_file, "rb").read)
#     embed(shot_b64, "image/png","Screenshot") 
# end
