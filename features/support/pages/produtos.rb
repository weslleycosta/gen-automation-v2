# classe para validar ofertas
class Produtos
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @botao_adicionar         = EL['botao_adicionar']
    @sinopse                 = EL['sinopse']
    @sumario                 = EL['sumario']
    @autoria                 = EL['autoria']
    @depoimentos             = EL['depoimentos']
    @amostra                 = EL['amostra']
    @compre_agora            = EL['compre_agora']
    @frete_pdp               = EL['frete_pdp']
    @calcular_frete          = EL['calcular_frete']
    @bookshelf               = EL['bookshelf']
    @genio                   = EL['genio']   
    @contador_mini_cart      = EL['contador_mini_cart']
    @bookshel_virtual_bundle = EL['bookshel_virtual_bundle']
    @genio_virtual_bundle    = EL['genio_virtual_bundle'] 
  end

  # def selecionando_oferta_filtrada
  #   binding.pry
  #   find(@botao_adicionar, wait: 10).click
  #   assert_selector(@contador_mini_cart, wait: 10)
  # end

  def abas
    find(@sinopse).click
    find(@sumario).click
    find(@autoria).click
    find(@depoimentos).click
    find(@amostra).click
  end

  def abas_virtual_impresso
    find(@sinopse).click
    find(@sumario).click
    find(@autoria).click
    find(@depoimentos).click
    find(@amostra).click
    find(@bookshelf).click
    find(@genio).click
  end

  def abas_virtual_virtual
    find(@sinopse).click
    find(@sumario).click
    find(@autoria).click
    find(@depoimentos).click
    find(@amostra).click
    find(@bookshel_virtual_bundle).click
    find(@genio_virtual_bundle).click
  end

  def compre_agora
    sleep 2
    find(@compre_agora).click
  end

  def frete
    find(@frete_pdp).set('04543000')
    find(@calcular_frete).click
    assert_text('Sedex')
    assert_text('Expresso')
  end
end
