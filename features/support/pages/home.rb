# classe para validar dados da home
class Home
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @logo = EL['logo']
    @cursos_ead = EL['cursos_ead']
    @negocio_gestao = EL['negocio_gestao']
  end

  def clica_logo
    find(@logo).click
    sleep 4
  end

  def clica_cursos
    find(@cursos_ead).click
    find(@negocio_gestao).click
  end
end
