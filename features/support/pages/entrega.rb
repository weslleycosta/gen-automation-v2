# classe para dados de endereco e entrega
class Entrega
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @campo_cep = EL['campo_cep']
    @campo_numero = EL['campo_numero']
    @campo_complemento = EL['campo_complemento']
    @campo_referencia = EL['campo_referencia']
    @campo_telefone = EL['campo_telefone']
    @frete_expresso = EL['frete_expresso']
    @proximo = EL['proximo']
    @selecionar_endereco = EL['selecionar_endereco']
    @botao_clique_seguir = EL['botao_clique_seguir']
    @endereco_diferente = EL['endereco_diferente']
  end

  
  def endereco_cadastrado
    find(@selecionar_endereco).click
  end

  def endereco_diferente_cobranca
    find(@endereco_diferente).click
  end

  def cadastro_endereco_ebook
    find(@campo_cep).set('09051510')
    find(@campo_numero).set('123')
    sleep 3
    find(@campo_telefone).set(1112121212)
    find(@botao_clique_seguir).click
  end

  def selecionar_endereco
    find(@selecionar_endereco).click
  end

  def proximo
    find(@proximo).click
  end
  def endereco_impresso
    sleep 5
    find(@campo_cep).set('04543000')
    find(@campo_numero).set('123')
    find(@campo_complemento).set('casa 10')
    find(@campo_telefone).set('1111111111')
    sleep 2
  end

  def frete_entrega
    find(@frete_expresso).click
    find(@proximo).click
  end
end
