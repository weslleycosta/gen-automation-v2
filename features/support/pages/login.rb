# classe que serve para validar dados de login
class Login
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @campo_email = EL['campo_email']
    @campo_senha = EL['campo_senha']
    @botao_entrar = EL['botao_entrar']
    @continuar = EL['continuar']
  end

  def login
    find(@campo_email).set('testeitudo@mailinator.com')
    find(@campo_senha).set('teste@123')
    find(@botao_entrar).click
    sleep 5
  end

  def botao_continuar
    find(@continuar).click
  end
end
