# classe pra validar campo busca
class Busca
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @campo_busca = EL['campo_busca']
    @botao_search = EL['botao_search']
  end

  def isbn_impresso
    find(@campo_busca).click
    sleep 1.5
    find(@campo_busca).native.send_keys('1234')
    find(@botao_search).click
  end

  def isbn_virtual
    find(@campo_busca).click
    sleep 3
    find(@campo_busca).native.send_keys('123456')
    find(@botao_search).click
  end

  def busca_titulo
    find(@campo_busca).click
    sleep 2
    find(@campo_busca).native.send_keys('e-Book do teste')
    find(@botao_search).click
    sleep 3
  end

  def busca_bundle
    find(@campo_busca).click
    sleep 3
    find(@campo_busca).native.send_keys('combo')
    find(@botao_search).click
  end

  def virtual_virtual
    find(@campo_busca).click
    sleep 3
    find(@campo_busca).native.send_keys('Teste virtual+virtual')
    find(@botao_search).click
  end

  def virtual_impresso
    find(@campo_busca).click
    sleep 3
    find(@campo_busca).native.send_keys('Combo impresso e virtual')
    find(@botao_search).click
  end
end
