#   classe que serve para validar dados do carrinho
class Carrinho
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @visualizar_carrinho = EL['visualizar_carrinho']
    @continuar = EL['continuar']
    @mini_cart = EL['mini_cart']
    @botao_fechar_pedido = EL['botao_fechar_pedido']
    @botao_cadastre = EL['botao_cadastre']
    @carrinho_quantidade = EL['carrinho_quantidade']
    @entrega_email = EL['entrega_email']
    @adicionar = EL['adicionar']
    @contador_mini_cart = EL['contador_mini_cart']
  end

  
  def clicando_carrinho
    assert_selector(@contador_mini_cart, wait: 10)
    find(@mini_cart).click
  end

  def adicionar_carrinho
    find(@adicionar).click
  end

  def cadastre
    sleep 2
    find(@botao_cadastre).click
  end

  def fechar_pedido
    assert_selector(@contador_mini_cart, wait: 10)
    find(@botao_fechar_pedido).click
    sleep 3
  end

  def quantidade_itens
    sleep 3
    cart_quant = find(@carrinho_quantidade, wait: 10)
    cart_quant.assert_text('2')
    find(@visualizar_carrinho).click
  end

  def entrega_email
    sleep 2
    find(@entrega_email).click
  end

  def botao_continuar
    find(@continuar, wait: 10).click
  end

  def  visualiza_carrinho
    find(@visualizar_carrinho).click
  end

  def adicionar_bundle
    find(@adicionar).click
    assert_selector(@contador_mini_cart, wait: 10)
    find(@mini_cart).click
    find(@visualizar_carrinho).click
  end

end
