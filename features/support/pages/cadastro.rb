# classe para validar cadastros de usuarios
class Cadastro
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @botao_entrar_home = EL['botao_entrar_home']
    @botao_criar_conta = EL['botao_criar_conta']
    @email_cadastro = EL['email_cadastro']
    @senha_cadastro = EL['senha_cadastro']
    @confirmar_senha = EL['confirmar_senha']
    @nome_cadastro = EL['nome_cadastro']
    @sobrenome_cadastro = EL['sobrenome_cadastro']
    @cpf_cadastro = EL['cpf_cadastro']
    @data_nascimento = EL['data_nascimento']
    @cel_cadastro = EL['cel_cadastro']
    @area_interesse = EL['area_interesse']
    @criar_conta_cadastro = EL['criar_conta_cadastro']
    @botao_pessoa_juridica = EL['botao_pessoa_juridica']
    @nome_fantasia = EL['nome_fantasia']
    @razao_social = EL['razao_social']
    @cnpj = EL['cnpj']
    @isento_inscricao = EL['isento_inscricao']
    @cadastre = EL['cadastre'] 
  end

  def entrar
    find(@botao_entrar_home).click
  end

  def criar_conta
    find(@botao_criar_conta).click
  end

  def cadastre
    sleep 3
    find(@cadastre, wait: 10).click
  end

  def cadastro_fisico
    find(@email_cadastro).set("#{FFaker::Name.first_name.downcase}@gmail.com.br")
    find(@senha_cadastro).set('teste@123')
    find(@confirmar_senha).set('teste@123')
    find(@nome_cadastro).set('Weslley')
    find(@sobrenome_cadastro).set('Tester')
    find(@cpf_cadastro).set(FFaker::IdentificationBR.cpf.to_s)
    find(@data_nascimento).set('10101997')
    find(@cel_cadastro).set('11999999999')
    find(@area_interesse).click
    find(@criar_conta_cadastro).click
    sleep 3
  end

  def cadastro_juridico
    texto = 'Minha Conta'

    find(@botao_pessoa_juridica).click
    find(@email_cadastro).set("#{FFaker::Name.first_name.downcase}@gmail.com.br")
    find(@senha_cadastro).set('teste@123')
    find(@confirmar_senha).set('teste@123')
    find(@nome_fantasia).set('Testers Costa')
    find(@razao_social).set('Weslley Tester')
    find(@cnpj).set(FFaker::IdentificationBR.cnpj.to_s)
    find(@isento_inscricao).click
    find(@area_interesse).click
    find(@criar_conta_cadastro).click
    sleep 3
    assert_text(texto)
  end
end
