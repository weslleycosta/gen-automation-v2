# classe para validar dados da tela de sucesso
class Sucesso
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @imprimir_boleto = EL['imprimir_boleto']
    @perfil = EL['perfil']
    @meus_pedidos = EL['meus_pedidos']
    @minha_conta = EL['minha_conta']
    @ver_detalhes = EL['ver_detalhes']
  end

  def impressao_boleto
    find(@imprimir_boleto).click
  end

  def meus_pedidos
    find(@perfil).click
    find(@meus_pedidos).click
    assert_text('Nº Pedido', wait: 10)
  end

  def minha_conta
    find(@perfil).click
    find(@minha_conta).click
    assert_text('Rua Santarém', wait: 10)
    assert_text('Avenida Presidente Juscelino', wait: 10)
  end

  def detalhes
    find(@ver_detalhes).click
  end

end
