#  classe para validar itens no catalago de busca
class CatalogoBusca
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @botao_adicionar = EL['botao_adicionar']
    @bundle_impresso = EL['bundle_impresso']
    @frete_pdp = EL['frete_pdp']
    @calcular_frete = EL['calcular_frete']
    @produto_titulo = EL['produto_titulo']
    @bundle_virtual = EL['bundle_virtual']
    @bundle_virtual_impresso = EL['bundle_virtual_impresso']
    @isbn_impresso = EL['isbn_impresso']
    @produto_ebook = EL['produto_ebook']
  end

  def adicionar_produto
    find(@botao_adicionar).click
  end

  def produto_isbn
    sleep 2
    find(@isbn_impresso, wait: 10).click
  end

  def produto_ebook
    find(@produto_ebook).click
  end

  def oferta_curso
    find(@botao_adicionar).click
    sleep 4
  end

  def oferta_bundle
    find(@bundle_impresso).click
  end

  def bundle_virtual
    find(@bundle_virtual).click
  end

  def bundle_virtual_impresso
    find(@bundle_virtual_impresso).click
  end
  
  def frete
    assert_selector(@frete_pdp, wait: 10)
    find(@frete_pdp).set('04543000')
  end

  def calcular_frete
    find(@calcular_frete).click
    assert_text('Sedex', wait: 10)
  end

  def produto_titulo
    find(@produto_titulo).click
  end
end
