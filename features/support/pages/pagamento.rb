#   classe que serve para validar dados de cartao de credito
class Pagamento
  include RSpec::Matchers
  include Capybara::DSL

  def initialize
    @numero_cartao            = EL['numero_cartao']
    @campo_nome               = EL['campo_nome']
    @campo_codigo_verificador = EL['campo_codigo_verificador']
    @select_mes               = EL['select_mes']
    @select_ano               = EL['select_ano']
    @select_parcelas          = EL['select_parcelas']
    @select_termos_condicoes  = EL['select_termos_condicoes']
    @botao_fazer_pedido       = EL['botao_fazer_pedido']
    @select_boleto            = EL['select_boleto']
    @termos_boleto            = EL['termos_boleto']
    @salvar_cartao            = EL['salvar_cartao']
    @select_cartao            = EL['select_cartao']
  end

  def inserir_cartao
    find(@numero_cartao).set('4000000000000010')
    find(@campo_nome).set('Weslley Teste')
    find(@campo_codigo_verificador).set('123')
    find(@select_mes).select('01 - janeiro')
    find(@select_ano).select('2022')
    sleep 2
    find(@select_parcelas, wait: 5).select('1x')
  end

  def seleciona_cartao
    find(@select_cartao).click
  end

  def salvar_cartao
    find(@salvar_cartao).click
  end

  def termo_fazer_pedido
    texto = 'Seu pedido foi recebido.'

    find(@select_termos_condicoes).click
    find(@botao_fazer_pedido).click
    assert_text(texto)
  end

  def boleto
    find(@select_boleto).click
    find(@termos_boleto).click
  end

  def finalizar_boleto
    texto = 'Imprimir Boleto'
    find(@botao_fazer_pedido).click
    sleep 5
    assert_text(texto)
  end
end
